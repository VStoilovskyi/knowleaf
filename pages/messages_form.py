from .messages_page import MessagesPage


class MessagesForm(MessagesPage):
    def __init__(self, driver):
        super().__init__(driver)
        self._message_text_field = lambda: self.find_clickable_element('//textarea[@name="message"]')
        self._send_message_button = lambda: self.find_clickable_element('//button[@type="submit"]')
        self._send_message_button_disabled = lambda: self.find_clickable_element('//button[@disabled]')
        self._unread_message_counter = lambda: self.find_element('//p[@class="unread-count"]')
        self.text1 = str

    def find_exact_text_field(self, text):
        self.find_clickable_element('//div[text()=' + text + ']')

    def send_message(self, text):
        self.text1 = text
        self._message_text_field().send_keys(text)
        self._send_message_button().click()
        text_field = self.find_element('//div[text()=' + '"' + text + '"]').text
        assert text_field == text
        return self.text1

    def check_unread_messages_counter(self):
        assert self._unread_message_counter().text == '1'

    def check_message_counter_disappeared(self):
        assert self._unread_message_counter() != '1'




