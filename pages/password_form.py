import time
from .password_page import EditPasswordPage


class UpdatePasswordForm(EditPasswordPage):
    def __init__(self, driver):
        super().__init__(driver)
        self._old_password_field = lambda: self.find_clickable_element('//input[@name="password"]')
        self._new_password_field = lambda: self.find_clickable_element('//input[@name="newPassword"]')
        self._confirm_password_field = lambda: self.find_clickable_element('//input[@name="confirmPassword"]')
        self._update_password_button = lambda: self.find_clickable_element('//button[@type="submit"]')
        self._error_message = lambda: self.find_elements_list('//p')

    def fill_update_password_form(self, old, new, confirm):
        self.fill_input(self._old_password_field, old)
        self.fill_input(self._new_password_field, new)
        self.fill_input(self._confirm_password_field, confirm)

    def click_update_password(self):
        self._update_password_button().submit()

    def check_error_message_present(self, error):
        time.sleep(0.6)
        assert self._error_message()[error]





