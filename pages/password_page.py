from .base_page import BasePage


class EditPasswordPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self._driver = driver
        self._screen_update_password_page = lambda: self.find_clickable_element('//a[@href="/app/edit-profile/password"]')

    def open_password_page(self):
        self._screen_update_password_page().click()

