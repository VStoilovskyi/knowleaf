from .base_page import BasePage


class AddProductPage(BasePage):
    def __init__(self, driver: object):
        super().__init__(driver)
        self._category_dropdown = lambda: self.find_clickable_element('//div[text()="Category"]')
        self._type_dropdown = lambda: self.find_clickable_element('//div[text()="Type"]')
        self._product_name_field = lambda: self.find_clickable_element('//input[@name="name"]')
        self._brand_name_field = lambda: self.find_clickable_element('//input[@name="brandName"]')
        self._stock_status_dropdown = lambda: self.find_clickable_element('//div[text()="In Stock"]')
        self._thc_field = lambda: self.find_clickable_element('//input[@name="thc"]')
        self._cbd_field = lambda: self.find_clickable_element('//input[@name="cbd"]')
        self._price_field = lambda: self.find_clickable_element('//input[@name="bid_price"]')
        self._qty_field = lambda: self.find_element('//input[@name="bid_size"]')
        self._units_dropdown = lambda: self.find_clickable_element('//div[text()="lbs"]')
        self._description_field = lambda: self.find_clickable_element('//textarea')
        self._image_inputs = lambda: self.find_elements_list('//input[@type="file"]')
        self._add_pricing_checkbox = lambda: self.find_clickable_element('//div[@class="Checkbox first-checkbox"]')
        self._allow_rfq_checkbox = lambda: self.find_clickable_element('//div[@class="Checkbox "]')
        self._add_another_product_button = lambda: self.find_clickable_element('//button[text()="+ Add Another Product"]')
        self._publish_status_off = lambda: self.find_clickable_element('//div[@class="variant "]')
        self._save_product_button = lambda: self.find_clickable_element('//button[text()="Save"]')
        self._close_block_icon_list = lambda: self.find_elements_list('//div[@class="IconClose"]')
        self._dropdown_inner_text = lambda: self.find_elements_list('//li')
        self._location = lambda: self.find_element('//input[@disabled]')
        self._error_message = lambda: self.find_elements_list('//p[contains(text(),"is required")]')

    def save_product(self):
        self._save_product_button().click()

    def set_publish_status_off(self):
        self._publish_status_off().click()

    def click_add_another_product(self):
        self._add_another_product_button().click()

    def delete_add_another_product(self):
        self._close_block_icon_list()[-1].click()

    def get_dropdown_inner_text(self):
        self._dropdown_inner_text()

    def select_category(self, product):
        self._category_dropdown().click()
        category_text = self._dropdown_inner_text()
        category_text[product['category'] + 2].click()

    def select_sub_category(self, product):
        self._type_dropdown().click()
        type_text = self._dropdown_inner_text()
        type_text[product['subCategory'] + 2].click()

    def select_stock_status(self, product):
        self._stock_status_dropdown().click()
        type_text = self._dropdown_inner_text()
        if product['stockStatus'] == 0:
            type_text[3].click()
        else:
            pass

    def set_publish_status(self, product):
        if product['publish'] == 0:
            self._publish_status_off().click()

    def fill_static_fields(self, product):
        self.fill_input(self._product_name_field, product['productName'])
        self.fill_input(self._brand_name_field, product['brandName'])
        self.fill_input(self._thc_field, product['thc'])
        self.fill_input(self._cbd_field, product['cbd'])
        if product['description'] != '':
            self.fill_input(self._description_field, product['description'])

    def allow_rfq(self, product):
        if product['rfq'] == 0:
            self._allow_rfq_checkbox().click()

    def upload_image(self, image_numbers, img):
        for elem in range(image_numbers):
            self._image_inputs()[elem].send_keys(img)

    def check_location(self, user):
        assert self._location().get_attribute('value') == user['location']

    def set_price(self, product):
        self._add_pricing_checkbox().click()
        self.fill_input(self._price_field, product['price'])
        self._qty_field().send_keys(product['qty'])
        self._units_dropdown().click()
        units_dropdown = self._dropdown_inner_text()
        if product['units'] == 'mL':
            units_dropdown[3].click()
        if product['units'] == 'units':
            units_dropdown[4].click()
        else:
            pass








