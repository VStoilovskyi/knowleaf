from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from constants import WAITING_TIMEOUT


class BasePage:
    def __init__(self, driver):
        self._driver = driver
        self._sign_in_button = lambda: self.find_clickable_element('//a[@href="/sign-in"]')
        self._logout_button = lambda: self.find_clickable_element('//a[@href="/logout"]')

    def fill_input(self, element, text):
        element().click()
        element().clear()
        element().send_keys(text)

    def find_clickable_element(self, locator, by=By.XPATH, timeout=WAITING_TIMEOUT):
        element = WebDriverWait(self._driver, timeout).until(
            EC.element_to_be_clickable((by, locator)),
            f'element with locator: {by}, {locator} is not present or clickable during {timeout}')
        return element

    def find_element(self, locator, by=By.XPATH, timeout=WAITING_TIMEOUT):
        element = WebDriverWait(self._driver, timeout).until(
            EC.presence_of_element_located((by, locator)),
            f'element with locator: {by}, {locator} is not present during {timeout}')
        return element

    def find_clickable_element_by_name(self, locator, by=By.NAME, timeout=WAITING_TIMEOUT):
        element = WebDriverWait(self._driver, timeout).until(
            EC.element_to_be_clickable((by, locator)),
            f'element with locator: {by}, {locator} is not present during {timeout}')
        return element

    def find_elements_list(self, locator, by=By.XPATH, timeout=WAITING_TIMEOUT):
        element = WebDriverWait(self._driver, timeout).until(
            EC.presence_of_all_elements_located((by, locator)),
            f'element with locator: {by}, {locator} is not present during {timeout}')
        return element

    def wait_until_spinner_present(self, timeout=WAITING_TIMEOUT):
        element = WebDriverWait(self._driver, timeout).until_not(
            EC.visibility_of_element_located((By.CLASS_NAME, 'Spinner'))
            )

    def click_sign_in_button(self):
        self._sign_in_button().click()






