from .base_page import BasePage
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from constants import WAITING_TIMEOUT




class EditPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self._driver = driver
        self._edit_profile_button = lambda: self.find_clickable_element('//div[@class="DashboardNavLink "][1] ')
        self._add_product_button = lambda: self.find_clickable_element('//div[@class="DashboardNavLink"][2]')
        self._my_cards_page_button = lambda: self.find_clickable_element('//a[@href="/app/edit-profile/cards"]')
        self._upload_avatar_button = lambda: self.find_clickable_element('//a[@href="/app/edit-profile/avatar"]')

    def open_edit_screen(self):
        self._edit_profile_button().click()

    def open_cards_screen(self):
        self._my_cards_page_button().click()

    def click_upload_avatar_button(self):
        self._upload_avatar_button().click()

    def open_add_product_page(self):
        self._add_product_button().click()

    def implicity_wait(self):
        self._driver.implicitly_wait(10)

    # def wait_until_spinner_present(self, timeout=WAITING_TIMEOUT):
    #     element = WebDriverWait(self._driver, timeout).until_not(
    #         EC.visibility_of_element_located((By.CLASS_NAME, 'Spinner'))
    #         )

