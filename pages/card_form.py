import time
from .edit_page import EditPage


class AddCardForm(EditPage):
    def __init__(self, driver):
        super().__init__(driver)
        self._ifames = lambda: self.find_elements_list('//iframe[@title="Secure payment input frame"]')
        self._add_button = lambda: self.find_clickable_element('//button[@type="submit"]')
        self._add_card_button = lambda: self.find_clickable_element('//*[@id="root"]/div[1]/div[3]/div/div[1]/button[2]')
        self._card_number = lambda: self.find_clickable_element_by_name("cardnumber")
        self._exp_date = lambda: self.find_clickable_element_by_name("exp-date")
        self._cvc = lambda: self.find_clickable_element_by_name("cvc")
        self._error_message = lambda: self.find_element('//p')
        self._delete_card_button = lambda: self.find_clickable_element('//*[@id="root"]/div[1]/div[3]/div/div[2]/div[2]/div[2]/div[1]/div/button[2]')

    def click_add_button(self):
        self._add_button().click()

    def click_add_new_card_button(self):
        self._add_card_button().click()

    def fill_iframe_form(self, field_type, number):
        frames = self._ifames()
        if field_type == 'cardnumber':
            self._driver.switch_to.frame(frame_reference=frames[0])
            field = self._card_number()
        elif field_type == 'cvc':
            self._driver.switch_to.frame(frame_reference=frames[2])
            field = self._cvc()
        elif field_type == "exp-date":
            self._driver.switch_to.frame(frame_reference=frames[1])
            field = self._exp_date()
        else:
            raise AttributeError
        field.click()
        field.clear()
        for elem in number:
            field.send_keys(elem)
            time.sleep(0.1)
        self._driver.switch_to.default_content()

    def check_error_is_present(self):
        assert self._error_message()

    def delete_card(self):
        self._delete_card_button().click()


