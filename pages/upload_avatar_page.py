from .edit_page import EditPage


class UploadAvatarPage(EditPage):
    def __init__(self, driver):
        super().__init__(driver)
        self._driver = driver
        self._add_avatar_button = lambda: self.find_clickable_element('//*[@id="root"]/div[1]/div[3]/div/div[2]/div[2]/button[2]')
        self._save_cropped_image = lambda: self.find_clickable_element('//button[text()="Save"]')
        self._image_input = lambda: self.find_element('//*[@accept="image/jpeg,image/jpg,image/png"]')
        self._dashboard_button = lambda: self.find_clickable_element('//a[text()="Dashboard"]')
        self._cancel_update_avatar_button = lambda: self.find_clickable_element('//button[@class="Button  remove-image  "]')
        self._spinner = lambda: self.find_clickable_element('//svg[@class="Spinner"]')

    def open_add_avatar_window(self, image_path):
        self._image_input().send_keys(image_path)

    def cropp_image(self):
        self._save_cropped_image().click()

    def click_upload_avatar(self):
        self._add_avatar_button().click()

    def check_is_uploaded(self):
        assert self._dashboard_button()

    def cancel_avatar_update(self):
        self._cancel_update_avatar_button().click()

    def check_cancel_button_present(self):
        assert self._cancel_update_avatar_button()


