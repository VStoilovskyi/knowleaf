from .base_page import BasePage
from pages import AddProductPage

class UploadCsvPage(AddProductPage):
    def __init__(self, driver):
        super().__init__(driver)
        self._download_template_button = lambda: self.find_clickable_element('//button[text()="Download Template"]')
        self._upload_file_button = lambda: self.find_clickable_element('//button[text()="Upload File"]')
        self._file_input = lambda: self.find_element('type="file"')
        self._product_name_column = lambda: self.find_elements_list('//input[@name="name"]')
        self._brand_name_column = lambda: self.find_elements_list('//input[@name="brandName"]')
        self._price_column = lambda: self.find_elements_list('//input[@name="price"]')
        self._qty_column = lambda: self.find_elements_list('//input[@name="size"]')
        self._description_column = lambda: self.find_elements_list('//input[@name="description"]')
        self._mapped_product_name_list = lambda: self.find_elements_list('//input[@name="name"]')
        self._mapped_brand_name_list = lambda: self.find_elements_list('//input[@name="brandName"]')
        self._mapped_price_list = lambda: self.find_elements_list('//input[@name="price"]')
        self._mapped_qty_list = lambda: self.find_elements_list('//input[@name="size"]')
        self._mapped_description_list = lambda: self.find_elements_list('//input[@name="description"]')

    def upload_csv(self, file):
        self._file_input().send_keys(file)

    def click_upload_button(self):
        self._upload_file_button().click()

    def check_mapped_qty(self, product_name, brand_name, price, qty, descr):
        assert len(self._mapped_product_name_list()) == product_name
        assert len(self._mapped_brand_name_list()) == brand_name
        assert len(self._mapped_price_list()) == price
        assert len(self._mapped_qty_list()) == qty
        assert len(self._mapped_description_list()) == descr





