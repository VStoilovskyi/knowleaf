from selenium.webdriver.common.by import By
from .base_page import BasePage


class SignInForm(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self._email_input = lambda: self.find_clickable_element('//input[@name="email"]')
        self._password_input = lambda: self.find_clickable_element('//input[@name="password"]')
        self._submit_button_active = lambda: self.find_clickable_element('//button[@type="submit"]')

    def fill_login_form(self, email, password):
        self.fill_input(self._email_input, email)
        self.fill_input(self._password_input, password)

    def click_submit(self):
        self._submit_button_active().click()

    def check_is_logged(self):
        assert self._logout_button


