from selenium.webdriver.common.by import By
from .base_page import BasePage


class ConfirmAge(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self._yes_button = lambda: self.find_clickable_element("//button[contains(text(),'Yes')]")

    def confirm_21(self):
        self._yes_button().click()


