from .base_page import BasePage


class MessagesPage(BasePage):
    def __init__(self, driver):
        super().__init__(driver)
        self._driver = driver
        self._conversation_list_item = lambda: self.find_clickable_element('//div[@class="SingleConversation "][1]')
        self._messages_button_dashboard = lambda: self.find_clickable_element('//div[@class="DashboardNavLink"][4]')
        self._unread_messages_total = lambda: self.find_elements_list('//p[@class="unread-count"]')
        self._message_preview_dashboard = lambda: self.find_clickable_element('//div[@class="SimpleMessage"][1]')

    def open_messages_screen(self):
        self._messages_button_dashboard().click()

    def select_last_conversation(self):
        self._conversation_list_item().click()

    def check_total_unread_counter(self):
        total = self._unread_messages_total()
        assert total[0].text == '8'

    def check_message_counter_dashboard(self):
        counter = self._unread_messages_total()
        assert counter[1].text == '1'

    def check_preview_message(self, text):
        preview_text = self._message_preview_dashboard().text.split()
        print(preview_text[4])
        assert preview_text[4] == text



