import time
from pages import AddCardForm, EditPage


def test_add_card_positive(logged_in01):
    a = EditPage(logged_in01)
    a.open_edit_screen()
    a.open_cards_screen()
    a = AddCardForm(logged_in01)
    time.sleep(2)
    a.click_add_new_card_button()
    a.fill_iframe_form('cardnumber', '4242424242424242')
    a.fill_iframe_form('cvc', '123')
    a.fill_iframe_form('exp-date', '0121')
    a.click_add_button()
    a.delete_card()


def test_add_card_check_errors_present(logged_in01):
    a = EditPage(logged_in01)
    a.open_edit_screen()
    a.open_cards_screen()
    a = AddCardForm(logged_in01)
    time.sleep(2)
    a.click_add_new_card_button()
    a.fill_iframe_form('cardnumber', '424242424242424')
    a.check_error_is_present()
    a.fill_iframe_form('cardnumber', '424242424242424')
    a.fill_iframe_form('exp-date', '0119')
    a.check_error_is_present()
