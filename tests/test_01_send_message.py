from pages import MessagesPage, MessagesForm
import time


message_text = 'Twist123456'


def test_send_message_existed_chat(logged_in01):
    a = MessagesPage(logged_in01)
    a.open_messages_screen()
    a.select_last_conversation()
    a = MessagesForm(logged_in01)
    a.send_message(message_text)


def test_message_receive(logged_in02):
    a = MessagesPage(logged_in02)
    a.check_total_unread_counter()
    a.check_message_counter_dashboard()
    a.check_preview_message(message_text)
    a.open_messages_screen()
    b = MessagesForm(logged_in02)
    b.check_unread_messages_counter()
    a.select_last_conversation()
    time.sleep(1.5)
    b.check_message_counter_disappeared()


