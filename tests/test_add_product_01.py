import time
from pages import AddProductPage, EditPage
from images import image
from test_data import PRODUCT_01, USER_01


def test_add_product_positive(logged_in01):
    a = EditPage(logged_in01)
    a.open_add_product_page()
    a = AddProductPage(logged_in01)
    time.sleep(1)
    a.select_category(PRODUCT_01)
    time.sleep(1)
    a.select_sub_category(PRODUCT_01)
    a.fill_static_fields(PRODUCT_01)
    a.check_location(USER_01)
    a.upload_image(5, image.jpeg)
    a.upload_image(4, image.png)
    a.upload_image(3, image.jpg)
    a.upload_image(2, image.jpeg)
    time.sleep(0.5)
    a.select_stock_status(PRODUCT_01)
    a.allow_rfq(PRODUCT_01)
    a.set_publish_status(PRODUCT_01)
    a.set_price(PRODUCT_01)
    a.click_add_another_product()
    a.delete_add_another_product()
    a.save_product()
    a.wait_until_spinner_present()


