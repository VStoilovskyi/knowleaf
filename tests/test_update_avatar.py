import time
from pages import EditPage, UploadAvatarPage
from images import image
import pytest


def test_update_avatar_positive(logged_in):
    a = EditPage(logged_in)
    time.sleep(4)
    a.open_edit_screen()
    time.sleep(4)
    a = UploadAvatarPage(logged_in)
    a.open_add_avatar_window(image.jpeg)
    a.cropp_image()
    a.check_cancel_button_present()
    a.click_upload_avatar()
    a.wait_until_spinner_present()

    a.open_add_avatar_window(image.jpg)
    a.cropp_image()
    a.check_cancel_button_present()
    a.click_upload_avatar()
    a.wait_until_spinner_present()

    a.open_add_avatar_window(image.png)
    a.cropp_image()
    a.check_cancel_button_present()
    a.click_upload_avatar()
    a.wait_until_spinner_present()

    a.open_add_avatar_window(image.png)
    a.cropp_image()
    a.cancel_avatar_update()


@pytest.mark.xfail
def test_update_avatar_negative_(logged_in):
    a = EditPage(logged_in)
    time.sleep(3)
    a.open_edit_screen()
    time.sleep(3)
    a = UploadAvatarPage(logged_in)
    a.open_add_avatar_window(image.gif)
    a.cropp_image()


