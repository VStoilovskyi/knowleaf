import time
from pages import EditPasswordPage, UpdatePasswordForm, EditPage


def test_update_password_positive(logged_in01):
    a = EditPage(logged_in01)
    a.open_edit_screen()
    time.sleep(4)
    a = UpdatePasswordForm(logged_in01)
    a.open_password_page()
    a.fill_update_password_form(old='Qwerty11', new='Qwerty111', confirm='Qwerty111')
    a.click_update_password()
    a.fill_update_password_form(old='Qwerty111', new='Qwerty11', confirm='Qwerty11')
    a.click_update_password()


def test_update_password_negative(logged_in01):
    a = EditPage(logged_in01)
    a.open_edit_screen()
    time.sleep(4)
    a = UpdatePasswordForm(logged_in01)
    a.open_password_page()
    a.fill_update_password_form(old='', new='Qwerty111', confirm='Qwerty111')
    a.check_error_message_present(0)
    a.fill_update_password_form(old='Qwerty11', new='', confirm='Qwerty111')
    a.check_error_message_present(1)
    a.check_error_message_present(2)
    a.fill_update_password_form(old='Qwerty11', new='Qwerty11', confirm='Qwerty111')
    a.check_error_message_present(1)
    a.check_error_message_present(2)
    a.fill_update_password_form(old='Qwerty11', new='Qwerty111', confirm='Qwerty11')
    a.check_error_message_present(1)
    a.check_error_message_present(2)
    a.fill_update_password_form(old='Qwerty11', new='Qwerty111', confirm='')
    a.check_error_message_present(2)