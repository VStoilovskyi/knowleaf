import os
import pytest
from selenium import webdriver
from constants import BASE_URL
from pages import ConfirmAge
from pages import BasePage, SignInForm


@pytest.fixture(scope="function")
def driver(request):
    wd = webdriver.Chrome(executable_path=f'{os.path.dirname(os.path.dirname(__file__))}/know_leaf/chromedriver')
    # wd = webdriver.Chrome()
    wd.maximize_window()
    wd.get(BASE_URL)
    c = ConfirmAge(wd)
    c.confirm_21()

    def close():
        wd.close()
    request.addfinalizer(close)
    return wd


@pytest.fixture(params=["vadym.stoilovskyi.cr+7@gmail.com"])
def logged_in01(request, driver):
    p = BasePage(driver)
    p.click_sign_in_button()
    p = SignInForm(driver)
    p.fill_login_form(email=request.param, password='Qwerty11')
    p.click_submit()
    return driver


@pytest.fixture(params=["vadym.stoilovskyi.cr+31@gmail.com"])
def logged_in02(request, driver):
    p = BasePage(driver)
    p.click_sign_in_button()
    p = SignInForm(driver)
    p.fill_login_form(email=request.param, password='Qwerty11')
    p.click_submit()
    return driver

