import os


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
PATH = os.path.join(os.path.sep, ROOT_DIR)


jpeg = f"{PATH + '/image.jpeg'}"
jpg = f"{PATH + '/image.jpg'}"
png = f"{PATH + '/image.png'}"
gif = f"{PATH + '/image.gif'}"
txt = f"{PATH + '/image.txt'}"
